let number1 = 0;
let number2 = 1;
let number3 = 0;
let total = 0;
let i = 0;
while (number1 + number2 < 4000000) {
  number3 = number1 + number2;
  number1 = number2;
  number2 = number3;

  if (number3 % 2 === 0) {
    total = total + number3;
  }
  i++;
}
console.log(total);
